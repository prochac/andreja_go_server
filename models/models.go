package models

import "strings"

var Schema = strings.Join([]string{
	productSchema,
	receiptSchema,
	receiptItemsSchema,
}, "")

var productSchema = `CREATE TABLE products (
	id	TEXT NOT NULL UNIQUE,
	name	TEXT NOT NULL,
	PRIMARY KEY(id)
);`

type Product struct {
	Id   string `db:"id"`
	Name string `db:"name"`
}

var receiptSchema = `CREATE TABLE receipts (
	id	TEXT NOT NULL UNIQUE,
	name	TEXT NOT NULL,
	PRIMARY KEY(id)
);`

type Receipt struct {
	Id           string        `db:"id"`
	Name         string        `db:"name"`
	ReceiptItems []ReceiptItem `db:"receipt_item"`
}

var receiptItemsSchema = `CREATE TABLE receipt_items (
	id	TEXT NOT NULL UNIQUE,
	amount	NUMBER NOT NULL,
	receipt_id	TEXT NOT NULL,
	product_id	TEXT NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(receipt_id) REFERENCES receipts(id),
	FOREIGN KEY(product_id) REFERENCES products(id)
);`

type ReceiptItem struct {
	Id        string  `db:"id"`
	ReceiptId string  `db:"receipt_id"`
	Amount    int     `db:"amount"`
	ProductId string  `db:"product_id"`
	Product   Product `db:"product"`
}
