package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/satori/go.uuid"
	"gitlab.com/prochac/andreja_go_server/models"
	"gitlab.com/prochac/andreja_go_server/storage"
)

func ProductListHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	pp := make([]models.Product, 0)
	err := storage.Db.Select(&pp, "SELECT * FROM products")
	if err != nil {
		ErrorHandler(w, err)
		return
	}
	b, err := json.Marshal(pp)
	if err != nil {
		ErrorHandler(w, err)
		return
	}
	JsonHandler(w, b)
}

func ProductByIdHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")
	var p models.Product
	err := storage.Db.Get(&p, "SELECT * FROM products WHERE id = ?", id)
	if err != nil {
		ErrorHandler(w, err)
		return
	}
	b, err := json.Marshal(p)
	if err != nil {
		ErrorHandler(w, err)
		return
	}
	JsonHandler(w, b)
}

func CreateProductHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	_, err := storage.Db.Exec("INSERT INTO products (id, name) VALUES (?, ?)", uuid.NewV4(), "new")
	if err != nil {
		ErrorHandler(w, err)
		return
	}
	w.WriteHeader(http.StatusCreated)
	JsonHandler(w, []byte{})
}

func DeleteProductHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")
	_, err := storage.Db.Exec("DELETE FROM products WHERE id = ?;", id)
	if err != nil {
		ErrorHandler(w, err)
		return
	}
	JsonHandler(w, []byte{})
}
