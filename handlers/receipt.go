package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/satori/go.uuid"
	"gitlab.com/prochac/andreja_go_server/models"
	"gitlab.com/prochac/andreja_go_server/storage"
)

func ReceiptListHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	pp := make([]models.Product, 0)
	err := storage.Db.Select(&pp, "SELECT * FROM receipts")
	if err != nil {
		ErrorHandler(w, err)
		return
	}
	b, err := json.Marshal(pp)
	if err != nil {
		ErrorHandler(w, err)
		return
	}
	JsonHandler(w, b)
}

func ReceiptByIdHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")
	var p models.Product
	err := storage.Db.Get(&p, "SELECT * FROM receipts WHERE id = ?", id)
	if err != nil {
		ErrorHandler(w, err)
		return
	}
	b, err := json.Marshal(p)
	if err != nil {
		ErrorHandler(w, err)
		return
	}
	JsonHandler(w, b)
}

func CreateReceiptHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	_, err := storage.Db.Exec("INSERT INTO receipts (id, name) VALUES (?, ?)", uuid.NewV4(), "new")
	if err != nil {
		ErrorHandler(w, err)
		return
	}
	w.WriteHeader(http.StatusCreated)
	JsonHandler(w, []byte{})
}

func DeleteReceiptHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")
	_, err := storage.Db.Exec("DELETE FROM receipts WHERE id = ?;", id)
	if err != nil {
		ErrorHandler(w, err)
		return
	}
	JsonHandler(w, []byte{})
}

func ReceiptItemListHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id := ps.ByName("id")
	var ri models.ReceiptItem
	err := storage.Db.Get(&ri, `
		SELECT 
			receipt_items.*,
			products.id AS "product.id",
			products.name AS "product.name"
		FROM 
			receipt_items
		JOIN 
			products ON receipt_items.product_id = products.id
		WHERE 
			receipt_items.receipt_id = ?`, id)
	if err != nil {
		ErrorHandler(w, err)
		return
	}
	b, err := json.Marshal(ri)
	if err != nil {
		ErrorHandler(w, err)
		return
	}
	JsonHandler(w, b)
}
