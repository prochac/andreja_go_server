package handlers

import (
	"database/sql"
	"fmt"
	"net/http"
)

func ErrorHandler(w http.ResponseWriter, err error) {
	switch err {
	case sql.ErrNoRows:
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprint(w, err)
	default:
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, err)
	}
}

func JsonHandler(w http.ResponseWriter, b []byte) {
	w.Header().Set("Content-Type", "application/json")
	w.Write(b)
}
