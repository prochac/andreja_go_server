package main

import (
	"log"
	"net/http"

	"github.com/jmoiron/sqlx"
	"github.com/julienschmidt/httprouter"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/prochac/andreja_go_server/handlers"
	"gitlab.com/prochac/andreja_go_server/models"
	"gitlab.com/prochac/andreja_go_server/storage"
)

func init() {
	var err error
	storage.Db, err = sqlx.Connect("sqlite3", "db.sqlite")
	if err != nil {
		log.Fatalln(err)
	}
	_, err = storage.Db.Exec(models.Schema)
	if err != nil {
		log.Println(err)
	}
}

func main() {
	defer storage.Db.Close()

	r := httprouter.New()

	// Products CRUD
	r.GET("/products", handlers.ProductListHandler)
	r.GET("/products/:id", handlers.ProductByIdHandler)
	r.POST("/products", handlers.CreateProductHandler)
	//r.PUT("/products/:id", handlers.UpdateProductHandler)
	r.DELETE("/products/:id", handlers.DeleteProductHandler)

	// Receipts CRUD
	r.GET("/receipts", handlers.ReceiptListHandler)
	r.GET("/receipts/:id", handlers.ReceiptByIdHandler)
	r.POST("/receipts", handlers.CreateReceiptHandler)
	//r.PUT("/receipts/:id", handlers.UpdateReceiptHandler)
	r.DELETE("/receipts/:id", handlers.DeleteReceiptHandler)
	r.GET("/receipts/:id/items", handlers.ReceiptItemListHandler)

	log.Fatal(http.ListenAndServe(":8080", r))
}
